package compiler.scanner;

public class Token {
	private TokenType type;
	private String text;
	
	public Token(TokenType type, String text) {
		super();
		this.type = type;
//		if(type == TokenType.CHAR)
//		{
//			text = text.substring(1, text.length()-1);
//			System.out.println(text);
//		}
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	
	public int getInt() {
		return Integer.parseInt(text);
	}

	public TokenType getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return "<[" + type + "]" + text + ">";
	}

	public String getPGenCompatibleString() {
		switch (type) {
		case EOF:
			return "$";
		case ID:
			return "id";
		case REAL:
			return "real";
		case INT:
			return "int";
		case CHAR:
			return "char";
		case STRING:
			return "str";
		case BOOL:
			return "bool";
		default:
			return text;
		}
	}
}
