package compiler.scanner;

public enum TokenType {
	INT,
	REAL,
	ID,
	CHAR,
	STRING,
	BOOL,
	EOF, 
	SYNTAX,
	OPERATOR,
	KEYWORD,
	HEX,
}
