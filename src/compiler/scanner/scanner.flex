/* JFlex example: partial Java language lexer specification */


%%

%public
%class Scanner
%function NextToken
%type Token
%unicode
%line
%column
%{
    StringBuffer string = new StringBuffer();

    public int lineNumber() {
        return yyline;
    }
%}

%eofval{
    return new Token(TokenType.EOF, "$");
%eofval}



LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
EndOfLine = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f\v]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

TraditionalComment   = "<--"  ~"-->"

// Comment can be the last line of the file, without line terminator.
EndOfLineComment     = "--" {InputCharacter}* {LineTerminator}?

DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

Identifier = [:jletter:] [:jletterdigit:]*

DecIntegerLiteral = [0-9]+
HexIntegerLiteral = 0[xX][0-9a-fA-F]*
RealLiteral = [0-9]+\.[0-9]* | [0-9]*\.[0-9]+
/* CharLiteral = '[^']' | '\'' */

SingleCharacter = [^\r\n\'\\]

%state STRING, CHARLITERAL

%%

/* keywords */
<YYINITIAL> "array"                     { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "assign"                    { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "bool"                      { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "break"                     { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "case"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "char"                      { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "continue"                  { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "do"                        { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "else"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "endcase"                   { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "environment"               { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "false"                     { return new Token(TokenType.BOOL, yytext()); }
<YYINITIAL> "function"                  { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "goto"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "if"                        { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "int"                       { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "isvoid"                    { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "label"                     { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "late"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "long"                      { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "of"                        { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "out"                       { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "real"                      { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "release"                   { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "return"                    { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "string"                    { return new Token(TokenType.ID, yytext()); }
<YYINITIAL> "structure"                 { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "true"                      { return new Token(TokenType.BOOL, yytext()); }
<YYINITIAL> "void"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "while"                     { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "read"                      { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "write"                     { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "strlen"                    { return new Token(TokenType.KEYWORD, yytext()); }
<YYINITIAL> "concat"                    { return new Token(TokenType.KEYWORD, yytext()); }



<YYINITIAL> {
  /* identifiers */
  {Identifier}                   { return new Token(TokenType.ID, yytext()); }

  /* literals */
  {HexIntegerLiteral}            { return new Token(TokenType.HEX, yytext()); }
  {RealLiteral}                  { return new Token(TokenType.REAL, yytext()); }
/*  {CharLiteral}                  { return new Token(TokenType.CHAR, yytext()); } */
  {DecIntegerLiteral}            { return new Token(TokenType.INT, yytext()); }
  \"                             { string.setLength(0); yybegin(STRING); }

  /* character literal */
  \'                             { yybegin(CHARLITERAL); }

  /* syntax */
  ","                            { return new Token(TokenType.SYNTAX, yytext()); }
  "."                            { return new Token(TokenType.SYNTAX, yytext()); }
  "["                            { return new Token(TokenType.SYNTAX, yytext()); }
  "]"                            { return new Token(TokenType.SYNTAX, yytext()); }
  ";"                            { return new Token(TokenType.SYNTAX, yytext()); }
  ":"                            { return new Token(TokenType.SYNTAX, yytext()); }
  "{"                            { return new Token(TokenType.SYNTAX, yytext()); }
  "}"                            { return new Token(TokenType.SYNTAX, yytext()); }
  "::"                           { return new Token(TokenType.SYNTAX, yytext()); }
  "("                            { return new Token(TokenType.SYNTAX, yytext()); }
  ")"                            { return new Token(TokenType.SYNTAX, yytext()); }

  /* operators */
  ":="                           { return new Token(TokenType.OPERATOR, yytext()); }

  "/"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "%"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "*"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "-"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "&"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "^"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "|"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "||"                           { return new Token(TokenType.OPERATOR, yytext()); }
  "&&"                           { return new Token(TokenType.OPERATOR, yytext()); }
  "+"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "~"                            { return new Token(TokenType.OPERATOR, yytext()); }
  "!"                            { return new Token(TokenType.OPERATOR, yytext()); }

  "="                            { return new Token(TokenType.OPERATOR, yytext()); }
  "<="                            { return new Token(TokenType.OPERATOR, yytext()); }
  ">="                            { return new Token(TokenType.OPERATOR, yytext()); }
  "!="                            { return new Token(TokenType.OPERATOR, yytext()); }
  "<"                            { return new Token(TokenType.OPERATOR, yytext()); }
  ">"                            { return new Token(TokenType.OPERATOR, yytext()); }




  /* comments */
  {Comment}                      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }
}

<STRING> {
  \"                             { yybegin(YYINITIAL);
                                   return
                                   new Token(TokenType.STRING, string.toString()); }
  [^\n\r\"\\]+                   { string.append( yytext() ); }
  \\t                            { string.append('\t'); }
  \\n                            { string.append('\n'); }

  \\r                            { string.append('\r'); }
  \\\"                           { string.append('\"'); }
  \\                             { string.append('\\'); }
}


<CHARLITERAL> {
  {SingleCharacter}\'            { yybegin(YYINITIAL); return new Token(TokenType.CHAR, yytext().charAt(0)+""); }
  /* escape sequences */
  "\\b"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\b");}
  "\\t"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\t");}
  "\\n"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\n");}
  "\\f"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\f");}
  "\\r"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\r");}
  "\\\""\'                       { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\"");}
  "\\'"\'                        { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\'");}
  "\\\\"\'                       { yybegin(YYINITIAL); return new Token(TokenType.CHAR, "\\"); }

  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {EndOfLine}                    { throw new RuntimeException("Unterminated character literal at end of line"); }
}


/* error fallback */
[^]                              { throw new Error("Illegal character <"+
                                                    yytext()+">"); }
