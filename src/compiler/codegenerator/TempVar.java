package compiler.codegenerator;

import compiler.codegenerator.instruction.AddressingMode;
import compiler.codegenerator.instruction.Operand;

/**
 * What can be in right hand side of an assignment (e.g. variables, arrays, immediates)
 */
public class TempVar {
	private String value;
	private TypeDesc typeDesc;
	private AddressingMode addressMode;
	
	public TempVar(AddressingMode addressMode, TypeDesc typeDesc, String value) {
		super();
		this.value = value;
		this.typeDesc = typeDesc;
		this.addressMode = addressMode;
	}
	
	public String getValue() {
		return value;
	}
	public TypeDesc getTypeDesc() {
		return typeDesc;
	}
	public AddressingMode getAddressMode() {
		return addressMode;
	}
	
	/**
	 * Converts an TempVar to an instruction operand (if it's a single basic variable)
	 */
	public Operand toOperand() {
		assert !typeDesc.isArray();
		SingleTypeDesc d = (SingleTypeDesc)typeDesc;
		assert !d.isStruct();
		return new Operand(addressMode, d.getBasicType().toOperandType(), value);
	}
	
	@Override
	public String toString() {
		return String.format("<TempVar %s, %s, %s>", value, addressMode, typeDesc);
	}
}
