package compiler.codegenerator;

import compiler.codegenerator.instruction.OperandType;

/**
 * Primitive types in language + struct 
 */
public enum BasicType {
	INTEGER(1, "int", OperandType.INTEGER),
	REAL(1, "real", OperandType.FLOAT),
	STRING(1, "string", OperandType.STRING),
	BOOL(1, "bool", OperandType.BOOL),
	CHARACTER(1, "char", OperandType.CHAR),
	STRUCT(-1, null, null);
	
	private int size;
	private SingleTypeDesc typeDesc;
	private OperandType operandType;
	private String typeNameInL;

	public String getTypeNameInL() {
		return typeNameInL;
	}

	private BasicType(int size, String TypeNameInL, OperandType operandType) {
		this.size = size;
		typeNameInL = TypeNameInL;
		this.operandType = operandType;
		typeDesc = new SingleTypeDesc(this);
	}

	public int getSize() {
		if (size <= 0)
			throw new RuntimeException("No Size for structs");
		return size;
	}
	
	public SingleTypeDesc getSingleTypeDesc() {
		return typeDesc;
	}
	
	public OperandType toOperandType() {
		return operandType;
	}
}