package compiler.codegenerator;

import java.util.HashMap;

import compiler.codegenerator.instruction.Dummy;
import compiler.codegenerator.instruction.Jump;
import compiler.codegenerator.instruction.MetaInstruction;

public class FunctionTable {
    HashMap<String, FunctionDesc> function_table = new HashMap<>();
    
    public FunctionTable() {
		// TODO Auto-generated constructor stub
	}
    
    public void addFunction(String name){
    	if(name == null)
    		name = "tmp";
    	function_table.put(name, new FunctionDesc());
    }
    
    public void setFunctionName(String name, String lastName){
    	if(lastName == null)
    		lastName = "tmp";
    	FunctionDesc funcDesc = function_table.remove(lastName);
    	function_table.put(name, funcDesc);
    }
    
    public void addRetDesc(TypeDesc typeDesc, String name){
    	getFunction(name).addRetDesc(typeDesc);
    }
    
    public void addArg(TypeDesc argDesc, String argName, String name){
    	getFunction(name).addArg(argDesc, argName);
    }
    
    public void setJumpTo(MetaInstruction jumpTo, String name){
    	getFunction(name).set_jumpTo(jumpTo);
    }
    
    public FunctionDesc getFunction(String name){
    	if(name == null)
    		name = "tmp";
    	return function_table.get(name);
    }
}
