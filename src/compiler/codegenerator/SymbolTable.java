package compiler.codegenerator;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Stores <name, TypeDesc> mappings and assigns a relative location to each of them. 
 * 
 * SymbolTable class used in different cases!
 * 1 - There is one for each scope
 * 2 - struct definitions are SymbolTable
 * 
 * Close the symbol table if you don't want to add new items into it.
 */
public class SymbolTable {
	private int location, startLocation;
	private HashMap<String, TypeDesc> descs = new HashMap<>();
	private HashMap<String, Integer> locations = new HashMap<>();
	private HashMap<Integer, String> locToName = new HashMap<>();
	private HashSet<String> notComplete = new HashSet<>();
	private boolean closed = false;
	private boolean functionBlock = false;

	public SymbolTable(int startLocation) {
		this.startLocation = location = startLocation;
		if(startLocation < 0 )
			functionBlock = true;
	}
	
	public boolean isFunctionBlock(){
		return functionBlock;
	}
	public int addVar(String varName, TypeDesc desc, boolean isComplete) {
		assert !hasVar(varName) : "Variable is present " + varName;
		assert !closed;
		if(!isComplete)
			notComplete.add(varName);
		descs.put(varName, desc);
		locations.put(varName, location);
		locToName.put(location, varName);
//		System.out.println("addVar " + varName + " @" + location);
		location += desc.getSize();
		return location - desc.getSize();
	}
	public int addVar(String varName, TypeDesc desc) {
		return addVar(varName, desc, true);
	}
	/**
	 * Adds a space to symbol table ram location (usually for other symbol tables inside it)
	 */
	public void addBubble(int size) {
		location += size;
	}
	
	public void setLastLocation(int loc){
		location = loc;
	}
	
	public int getNextSize(int loc){
		return descs.get(locToName.get(loc)).getSize();
	}
	public String getVarName(int loc){
		return locToName.get(loc);
	}

	/**
	 * @return location of a variable relative to beginning of symbol table
	 */
	public int getLocation(String varName) {
		assert hasVar(varName) : "Variable not present " + varName;
		return locations.get(varName);
	}
	
	public boolean isComplete(String varName){
		return !notComplete.contains(varName);
	}
	public void setComplete(String varName){
		notComplete.remove(varName);
	}

	/**
	 * @return descriptor for this name or null if not in SymbolTable
	 */
	public TypeDesc getTypeDesc(String varName) {
		return descs.get(varName);
	}

	public boolean hasVar(String varName) {
		return getTypeDesc(varName) != null;
	}
	
	public int getCurrentEnd() {
		return location;
	}

	public int getEnd() {
		assert closed;
		return location;
	}
	
	public int getSize() {
		return getEnd() - startLocation;
	}
	
	public void close() {
		closed = true;
	}
}
