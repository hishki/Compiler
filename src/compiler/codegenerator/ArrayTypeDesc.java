package compiler.codegenerator;

import java.util.ArrayList;

/**
 * Array type descriptors. For `my_struct[10][20][3]' underlying type is `my_struct' and dimensions (10, 20, 3) 
 */
public class ArrayTypeDesc extends TypeDesc {
	private ArrayList<TempVar> arrayDimensions;
	private SingleTypeDesc underlyingType;
	private TempVar tempVarSize = null;
	public ArrayList<TempVar> getArrayDimensions() {
		return arrayDimensions;
	}

	public SingleTypeDesc getUnderlyingType() {
		return underlyingType;
	}

	public ArrayTypeDesc(ArrayList<TempVar> arrayDimensions, SingleTypeDesc underlyingType) {
		super();
		this.arrayDimensions = arrayDimensions;
		this.underlyingType = underlyingType;
	}
	
	public void addDim(TempVar dim){
		arrayDimensions.add(dim);
	}
	public void resetDim(){
		arrayDimensions.clear();
		tempVarSize = null;
	}
	@Override
	public boolean isArray() {
		return true;
	}
	
	@Override
	public int getSize() {
//		return 3;
		return arrayDimensions.size()*1 + 1 + 1;
	}
	
	@Override
	public String toString() {
		return "Array of " + underlyingType;
	}

	public void setTempVarSize(TempVar size) {
		// TODO Auto-generated method stub
		tempVarSize = size;
	}
	
	public TempVar getTempVarSize(){
		return tempVarSize;
	}
}