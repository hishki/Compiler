package compiler.codegenerator;

import java.util.ArrayList;

/**
 * Every type descriptor fits in this class, like int, float[10], my_struct[10][20] 
 */
public abstract class TypeDesc {
	public abstract boolean isArray();
	public abstract int getSize();
}
