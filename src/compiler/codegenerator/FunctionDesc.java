package compiler.codegenerator;

import java.util.ArrayList;

import compiler.codegenerator.instruction.MetaInstruction;

public class FunctionDesc {
	MetaInstruction jumpTo;
	ArrayList<TypeDesc> retDesc = new ArrayList<>();
	ArrayList<TypeDesc> argDesc = new ArrayList<>();
	ArrayList<String> argName = new ArrayList<>();
	public FunctionDesc() {
		
	}
	
	public void set_jumpTo(MetaInstruction jumpTo){
		this.jumpTo = jumpTo;
	}
	
	public MetaInstruction get_jumpTo(){
		return jumpTo;
	}
	
	public boolean isVoid(){
		return retDesc.size() == 0;
	}
	
	public void addRetDesc(TypeDesc typeDesc){
		retDesc.add(typeDesc);
	}
	
	public void addArg(TypeDesc argDesc, String argName){

		this.argDesc.add(argDesc);
		this.argName.add(argName);
	}
	
	public ArrayList<String> getArgName(){
		return argName;
	}
	public ArrayList<TypeDesc> getArgTypeDesc(){
		return argDesc;
	}
	
	public int getArgSize(){
		int ret=0;
//		for(TypeDesc type: retDesc)
//			ret += type.getSize();
		for(TypeDesc type: argDesc)
			ret += type.getSize();
		return ret;
	}
	
	public int getRetSize(){
		int ret=0;
		for(TypeDesc type: retDesc)
			ret += type.getSize();
		return ret;
	}
	
	public ArrayList<TypeDesc> getRetType(){
		return retDesc;
	}
	
	public int getRetLoc(int i){
		int ret = getArgSize() + 2;
		for(int j=i; j<retDesc.size(); j++)
			ret += retDesc.get(j).getSize();
		return -ret;
	}
}
