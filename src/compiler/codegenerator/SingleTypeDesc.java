package compiler.codegenerator;

/**
 * Type descriptors that are not array (i.e. only a single primitive type or struct) 
 */
public class SingleTypeDesc extends TypeDesc {
	private BasicType basicType=BasicType.INTEGER;
	private SymbolTable structDesc;
	
	public BasicType getBasicType() {
		return basicType;
	}

	public SingleTypeDesc(BasicType basicType) {
		this.basicType = basicType;
		assert !isStruct();
	}
	
	public SingleTypeDesc(SymbolTable structDesc) {
		this.structDesc = structDesc;
		this.basicType = BasicType.STRUCT;
	}
	
	public SymbolTable getStructDesc() {
		return structDesc;
	}

	public boolean isStruct() {
		return basicType == BasicType.STRUCT;
	}

	@Override
	public boolean isArray() {
		return false;
	}

	@Override
	public int getSize() {
		if (isStruct())
			return 1;
//			return structDesc.getSize();
		else
			return basicType.getSize();
	}
	
	@Override
	public String toString() {
		if (isArray())
			return "Array of " + basicType.toString(); 
		return basicType.toString();
	}
}