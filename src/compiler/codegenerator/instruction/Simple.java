package compiler.codegenerator.instruction;

/**
 * Simple one line instruction that do not change
 */
public class Simple extends OneLine {
	private Instruction inst;
	
	public Simple(String opCode, Operand op1, Operand op2, Operand op3) {
		inst = new Instruction(opCode, op1, op2, op3);
	}
	public Simple(String opCode, Operand op1, Operand op2) {
		inst = new Instruction(opCode, op1, op2, null);
	}
	public Simple(String opCode, Operand op1) {
		inst = new Instruction(opCode, op1, null, null);
	}
	public Simple(String opCode) {
		inst = new Instruction(opCode, null, null, null);
	}
	
	private Operand lateOp = null;
	private MetaInstruction miOp = null;
	public Simple(String opCode, MetaInstruction op1, Operand op2, Operand op3){
		lateOp = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0+"");
		miOp = op1;
		inst = new Instruction(opCode, lateOp, op2, op3);
	}
	
	@Override
	public Instruction getInstruction() {
		if(lateOp != null){
			lateOp.setValue(miOp.getFirstLine() + "");
		}
		return inst;
	}
}