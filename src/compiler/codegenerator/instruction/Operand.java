package compiler.codegenerator.instruction;

/**
 * Represents an operand of an instruction. Can be an immediate, int variable, pointer to float, etc 
 */
public class Operand {
	/**
	 * Value for immediates, location for directs, location of pointer for indirects
	 */
	private String value;
	private OperandType varType;
	private AddressingMode addressMode;
	
	public void setValue(String value){
		this.value = value;
	}
	public String getValue() {
		return value;
	}

	public OperandType getVarType() {
		return varType;
	}

	public AddressingMode getAddressMode() {
		return addressMode;
	}

	public Operand(AddressingMode addressMode, OperandType varType, String value) {
		this.addressMode = addressMode;
		this.varType = varType;
		this.value = value;
	}
	
	public String toString() {
		if (addressMode == AddressingMode.IMMEDIATE)
			return String.format("#%s", value);
		return String.format("%s%s(%s)", addressMode.getOp(), varType.getOp(), value);
	}
	
	public String toInstructionFormat() {
		if(varType == null)
			varType = OperandType.INTEGER;
		String s= varType.getOp();
		if(s.equals("s"))
			s = "i";
		if(s.equals("p"))
			s = "s";
		return String.format("%s_%s_%s", addressMode.getOp(), s, value);
	}
}