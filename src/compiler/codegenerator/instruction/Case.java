package compiler.codegenerator.instruction;

import java.util.ArrayList;

import compiler.codegenerator.BasicType;
import compiler.codegenerator.TempVar;

public class Case{

	TempVar condVar, tmpVar, tmpBool;
	MetaInstruction endDummy, beginDummy;
	int conditionPosition = 0;
	int minVal=1000000, maxVal=-100000;
	ArrayList<Integer> values = new ArrayList<>();
	ArrayList<MetaInstruction> beginDummies = new ArrayList<>();
	public Case(TempVar condVar, TempVar tmpVar, TempVar tmpBool){
		this.condVar = condVar;
		this.tmpVar = tmpVar;
		this.tmpBool = tmpBool;
		endDummy = new Dummy();
	}

	public void addCase(String val, MetaInstruction beginDummy){
		int value = Integer.parseInt(val);
		values.add(value);
		if(value < minVal)
			minVal = value;
		if(value > maxVal)
			maxVal = value;
		beginDummies.add(beginDummy);
		return;
	}
	
	public MetaInstruction getEndDummy(){
		return endDummy;
	}
	
	
	ArrayList<MetaInstruction> getConditionVariable(){
		ArrayList<MetaInstruction> ret = new ArrayList<>();
		ret.add(new Simple("-", condVar.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, minVal+""), tmpVar.toOperand()));
		ret.add(new Simple(">=", tmpVar.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0+""), tmpBool.toOperand()));
		ret.add(new Jump(endDummy, tmpBool.toOperand()));
		ret.add(new Simple("<=", tmpVar.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, maxVal-minVal+""), tmpBool.toOperand()));
		ret.add(new Jump(endDummy, tmpBool.toOperand()));
		Dummy beginDummy = new Dummy();
		ret.add(new Simple("+", beginDummy, tmpVar.toOperand(), tmpVar.toOperand()));
		ret.add(new Simple("jmp", tmpVar.toOperand()));
		ret.add(beginDummy);
		return ret;
//		instructions.add(new Jump(dummy, null));
	}
	
	public ArrayList<MetaInstruction> getJumpTable(){
		ArrayList<MetaInstruction> jumpTable = getConditionVariable();
		for(int i=minVal; i<=maxVal; i++){
			int idx = values.indexOf(i);
			if(idx == -1)
				jumpTable.add(new Jump(endDummy, null));
			else
				jumpTable.add(new Jump(beginDummies.get(idx), null));
		}
		return jumpTable;
	}

	public void addConditionPosition(int size) {
		conditionPosition = size;
	}
	public int getConditionPosition() {
		return conditionPosition;
	}

}
