package compiler.codegenerator.instruction;

/**
 * An instance of this class shows a single instruction in our middle code ISA 
 */
public class Instruction {
	private String opCode;
	private Operand op1, op2, op3;
	
	public Instruction(String opCode, Operand op1, Operand op2, Operand op3) {
		this.opCode = opCode;
		this.op1 = op1;
		this.op2 = op2;
		this.op3 = op3;
	}
	
	public String toString() {
		assert (op1 == null && op2 == null && op3 == null) || 
			   (op1 != null && op2 == null && op3 == null) ||
			   (op1 != null && op2 != null && op3 == null) ||
			   (op1 != null && op2 != null && op3 != null);
		
		StringBuilder sb = new StringBuilder();
		sb.append(opCode.length() == 1 ? opCode + " " : opCode);
		if (op1 != null)
			sb.append(" " + op1.toString());
		if (op2 != null)
			sb.append(" " + op2.toString());
		if (op3 != null)
			sb.append(" " + op3.toString());
		return sb.toString();
	}
	
	public String toInstructionFormat() {
		assert (op1 == null && op2 == null && op3 == null) || 
		   (op1 != null && op2 == null && op3 == null) ||
		   (op1 != null && op2 != null && op3 == null) ||
		   (op1 != null && op2 != null && op3 != null);
		
		StringBuilder sb = new StringBuilder();
		sb.append(opCode);
		if (op1 != null)
			sb.append(" " + op1.toInstructionFormat());
		if (op2 != null)
			sb.append(" " + op2.toInstructionFormat());
		if (op3 != null)
			sb.append(" " + op3.toInstructionFormat());
		return sb.toString();
	}
}
