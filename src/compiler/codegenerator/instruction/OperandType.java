package compiler.codegenerator.instruction;

public enum OperandType {
	INTEGER,
	FLOAT,
	BOOL,
	STRING,
	CHAR,
	pert;
	
	public String getOp() {
		return this.toString().toLowerCase().substring(0, 1);
	}
	
	public static OperandType reverse(String op) {
		for (OperandType a: OperandType.values())
			if (a.getOp().equals(op))
				return a;
		throw new RuntimeException("Not Found");
	}
}