package compiler.codegenerator.instruction;

import java.util.ArrayList;


/**
 * Instead of creating instructions and edit them afterward (e.g. forward
 * jumps) we use MetaInstructions. A MetaInstruction will expand to one or more
 * real instructions in the last phase of code generation. This can give us a
 * lots of freedom in creating pseudo instructions and issues like forward
 * jumping.
 */
public abstract class MetaInstruction {
	private int firstLine;

	public abstract int getLineCount();
	public abstract ArrayList<Instruction> getInstructions();

	public int getFirstLine() {
		return firstLine;
	}

	public void setFirstLine(int firstLine) {
		this.firstLine = firstLine;
	}
}
