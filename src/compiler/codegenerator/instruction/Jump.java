package compiler.codegenerator.instruction;


/**
 * Converts to a jz or jmp instruction (If your condition is null it's a jmp)
 * You could mark a MetaInstruction to jump to or after it 
 */
public class Jump extends OneLine {
	private MetaInstruction target;
	private boolean jumpAfter;
	private Operand condition;

	public Jump(MetaInstruction target, Operand condition, boolean jumpAfter) {
		assert target != null;
		
		this.target = target;
		this.condition = condition;
		this.jumpAfter = jumpAfter;
	}
	
	public void setTarget(MetaInstruction target) {
		this.target = target;
	}

	public Jump(MetaInstruction target, Operand condition) {
		this(target, condition, false);
	}
	
	@Override
	public Instruction getInstruction() {
		int l = target.getFirstLine();
		if (jumpAfter)
			l += target.getLineCount();
		
		Operand location = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + l);
		
		if (condition != null)
			return new Instruction("jz", condition, location, null);
		else
			return new Instruction("jmp", location, null, null);
	}

}
