package compiler.codegenerator.instruction;

public enum AddressingMode {
	GLOBAL_DIRECT("gd"),
	GLOBAL_INDIRECT("gi"),
	LOCAL_DIRECT("ld"),
	LOCAL_INDIRECT("li"),
	IMMEDIATE("im");
	
	private String op;
	
	private AddressingMode(String str) {
		this.op = str;
	}
	
	public String getOp() {
		return op;
	}
	
	public static AddressingMode reverse(String op) {
		for (AddressingMode a: AddressingMode.values())
			if (a.getOp().equals(op))
				return a;
		throw new RuntimeException("Not Found");
	}
	
	public boolean isGlobal() {
		return this == GLOBAL_DIRECT || this == GLOBAL_INDIRECT;
	}
	
	public boolean isIndirect() {
		return this == GLOBAL_INDIRECT || this == LOCAL_INDIRECT;
	}
}