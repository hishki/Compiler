package compiler.codegenerator.instruction;

import java.util.ArrayList;

import compiler.codegenerator.ArrayTypeDesc;
import compiler.codegenerator.TempVar;;

public class MakeArray extends MetaInstruction {
	ArrayList<Instruction> instructions = new ArrayList<>();
	
	public MakeArray(ArrayTypeDesc arrayTypeDesc, TempVar size, int location){
		instructions.add(
				new Simple(":=", 
						arrayTypeDesc.getArrayDimensions().get(0).toOperand(),
						size.toOperand()).getInstruction());
		for(int i=1; i<arrayTypeDesc.getArrayDimensions().size(); i++){
			TempVar tmp= arrayTypeDesc.getArrayDimensions().get(i);
			Instruction instruction  = new Simple("*", tmp.toOperand(), size.toOperand(), size.toOperand()).getInstruction();
			instructions.add(instruction);
		}
		Operand type_size = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + arrayTypeDesc.getUnderlyingType().getSize());
		Instruction instruction  = new Simple("*", type_size, size.toOperand(), size.toOperand()).getInstruction();
		instructions.add(instruction);
		Operand loc = new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, ""+location);
		instructions.add(new Simple("gmm", size.toOperand(), loc).getInstruction());
		arrayTypeDesc.setTempVarSize(size);
	}
	@Override
	public int getLineCount() {
		// TODO Auto-generated method stub
		return instructions.size();
	}

	@Override
	public ArrayList<Instruction> getInstructions() {

		return instructions;
	}

}
