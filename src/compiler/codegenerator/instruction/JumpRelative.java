package compiler.codegenerator.instruction;

public class JumpRelative extends OneLine {
	private Operand condition;
	private int instructionCount;

	public JumpRelative(Operand condition, int instructionCount) {
		this.condition = condition;
		this.instructionCount = instructionCount;
	}
	
	@Override
	public Instruction getInstruction() {
		int l = getFirstLine() + instructionCount;
		Operand location = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + l);
		
		if (condition != null)
			return new Instruction("jz", condition, location, null);
		else
			return new Instruction("jmp", location, null, null);
	}

}
