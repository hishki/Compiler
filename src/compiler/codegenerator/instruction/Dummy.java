package compiler.codegenerator.instruction;

import java.util.ArrayList;

public class Dummy extends MetaInstruction {
	@Override
	public int getLineCount() {
		return 0;
	}

	@Override
	public ArrayList<Instruction> getInstructions() {
		return new ArrayList<>();
	}

}
