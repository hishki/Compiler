package compiler.codegenerator.instruction;

import java.util.ArrayList;

public abstract class OneLine extends MetaInstruction {
	@Override
	public int getLineCount() {
		return 1;
	}

	@Override
	public ArrayList<Instruction> getInstructions() {
		ArrayList<Instruction> a = new ArrayList<>();
		a.add(getInstruction());
		return a;
	}

	protected abstract Instruction getInstruction();
}
