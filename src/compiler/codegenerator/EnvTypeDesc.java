package compiler.codegenerator;

public class EnvTypeDesc extends TypeDesc {
	private SymbolTable envSymbolTable;
	
	public SymbolTable getEnvSymbolTable(){
		return envSymbolTable;
	}
	
	public EnvTypeDesc(int begin) {
		super();
		envSymbolTable = new SymbolTable(begin);
	}
	
	public void addVar(String varName, TypeDesc desc){
		envSymbolTable.addVar(varName, desc);
	}
	
	//return null if has not
	public TypeDesc getVar(String varName){
		return envSymbolTable.getTypeDesc(varName);
	}
	
	public void closeEnv(){
		envSymbolTable.close();
	}
	
	@Override
	public boolean isArray() {
		return false;
	}
	
	@Override
	public int getSize() {
		return envSymbolTable.getSize();
	}
	
	@Override
	public String toString() {
		return "enviroment ";
	}
}