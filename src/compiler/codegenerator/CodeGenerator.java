package compiler.codegenerator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import compiler.codegenerator.instruction.AddressingMode;
import compiler.codegenerator.instruction.Case;
import compiler.codegenerator.instruction.Dummy;
import compiler.codegenerator.instruction.Instruction;
import compiler.codegenerator.instruction.Jump;
import compiler.codegenerator.instruction.JumpRelative;
import compiler.codegenerator.instruction.MakeArray;
import compiler.codegenerator.instruction.MetaInstruction;
import compiler.codegenerator.instruction.Operand;
import compiler.codegenerator.instruction.OperandType;
import compiler.codegenerator.instruction.Simple;
import compiler.error.CodeError;
//import compiler.scanner.CharacterClass;
import compiler.scanner.Scanner;
import compiler.scanner.Token;

public class CodeGenerator 
{
    Scanner scanner;
    
    public void exceptionHandler(Exception exp) throws Exception{
//    	throw(exp);
    	System.out.println(exp.getMessage());
    	return;
    }
    public CodeGenerator(Scanner scanner) {
        this.scanner = scanner;
        
        for (BasicType b:BasicType.values())
        	if (b != BasicType.STRUCT)
        		types.put(b.getTypeNameInL(), b.getSingleTypeDesc());
    }
    
    public void pushId(String text) {
    	idStack.push(text);
    }
    
    ArrayList<MetaInstruction> instructions = new ArrayList<>();
    Stack<String> idStack = new Stack<>();
    Stack<TempVar> sematicStack = new Stack<>();
    SymbolTable globalSymbolTable = new SymbolTable(0);
    Stack<SymbolTable> symbolTableStack = new Stack<>();
    HashMap<String, TypeDesc> types = new HashMap<>();
    MetaInstruction endInstructions = new Simple(":=", new Operand(AddressingMode.LOCAL_DIRECT ,OperandType.INTEGER, 0+""), new Operand(AddressingMode.LOCAL_DIRECT ,OperandType.INTEGER, 0+""));
    int tmpCount = 1;
    
    private TempVar getTempVar(String tmpTag, TempVar op1) {
    	return getTempVar(tmpTag, op1, op1);
    }
    
    private TempVar getTempVar(String tmpTag, TempVar op1, TempVar op2) {
//    	assert !op1.getTypeDesc().isArray();
    	SingleTypeDesc d1 = (SingleTypeDesc) op1.getTypeDesc();
//    	assert !d1.isStruct();
    	
//    	assert !op2.getTypeDesc().isArray();
    	SingleTypeDesc d2 = (SingleTypeDesc) op1.getTypeDesc();
//    	assert !d2.isStruct();
    	
    	return getTempVar(tmpTag, op1.getTypeDesc(), op2.getTypeDesc());
    }
    
    private TempVar getTempVar(String tmpTag, TypeDesc t1) {
    	return getTempVar(tmpTag, t1, t1);
    }
    
    private TempVar getTempVar(String tmpTag, TypeDesc t1, TypeDesc t2) {
    	TypeDesc resDesc = null;
    	if (t1 == t2)
    		resDesc = t1;
    	else {
    		resDesc = t1;
//    		assert false : "Not compatible:" + t1 + " " + t2;
    	}
    	
    	if(!symbolTableStack.isEmpty())
    	{
	    	int loc = symbolTableStack.peek().addVar(String.format("tmp_%s_%d", tmpTag, tmpCount++), resDesc);
	    	if(BasicType.BOOL.getSingleTypeDesc() == resDesc || BasicType.CHARACTER.getSingleTypeDesc() == resDesc)
	    		instructions.add(new Simple(":=", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0 + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, loc+"")));
	    	return new TempVar(AddressingMode.LOCAL_DIRECT, t1, "" + loc);
    	}else{
	    	int loc = globalSymbolTable.addVar(String.format("tmp_%s_%d", tmpTag, tmpCount++), resDesc);
	    	if(BasicType.BOOL.getSingleTypeDesc() == resDesc || BasicType.CHARACTER.getSingleTypeDesc() == resDesc)
	    		instructions.add(new Simple(":=", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0 + ""), new Operand(AddressingMode.GLOBAL_DIRECT, OperandType.INTEGER, loc+"")));
	    	return new TempVar(AddressingMode.GLOBAL_DIRECT, t1, "" + loc);
    	}
    }
    
    private SymbolTable findInSymbolTable(String varName) {
    	for (int i = symbolTableStack.size() - 1; i >= 0; i--)
    	{
    		if (symbolTableStack.get(i).hasVar(varName))
    			return symbolTableStack.get(i);
    		if(symbolTableStack.get(i).isFunctionBlock())
    			return null;
    	}
    	return null;
    }
    
    /**
     * @param name name of a basic type or a struct
     * @return a TypeDesc for it
     * @throws Exception 
     */
    private SingleTypeDesc getTypeDescForName(String name) throws Exception {
    	SingleTypeDesc t = (SingleTypeDesc) types.get(name);
    	if (t == null)
    		exceptionHandler(new  CodeError("Unknown type: " + name, "on line " + scanner.lineNumber()));
    	return t;
    }
    
    FunctionTable functionTable = new FunctionTable();
    Stack<Integer> returnCounter = new Stack<>();
    String functionName = null;
    MetaInstruction jmpMain = null;
    private boolean functionCg(String sem, Token token) throws Exception {
    	String varName, type;
    	TypeDesc typeDesc;
    	switch (sem) {
    	case "functionBegin":
    		functionTable.addFunction(null);
    		inFunctionBlock = true;
    		break;
    	
    	case "functionName":
    		functionName = idStack.pop();
    		Dummy dummy = new Dummy();
    		instructions.add(dummy);
    		MetaInstruction jmp = new Jump(dummy, null);
    		if(functionName.equals("main"))
    			jmpMain = jmp;
    		functionTable.setJumpTo(jmp, null);
    		functionTable.setFunctionName(functionName, null);

    		break;
    		
    	case "functionAddRet":
    		type = idStack.pop();
    		typeDesc = getTypeDescForName(type);
    		functionTable.addRetDesc(typeDesc, null);
    		break;

    	case "functionAddArg":
    		varName = idStack.pop();
    		type = idStack.pop();
    		ensureNonVoid(type);
    		typeDesc = getTypeDescForName(type);
    		functionTable.addArg(typeDesc, varName, functionName);

    		break;
    		
    	case "functionEnd":
    		if(functionName.equals("main"))
    			instructions.add(new Jump(endInstructions, null));
    		else
    			instructions.add(new Simple("jmp", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, -1 + "")));
    		break;
    	
    	case "returnBegin":
    		returnCounter.push(0);
    		break;
    	
    	case "returnVal":
    		String id = idStack.pop();
    		SymbolTable localST = findInSymbolTable(id);
    		int cnt = returnCounter.pop();
    		FunctionDesc func = functionTable.getFunction(functionName);
    		sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, func.getRetType().get(cnt), func.getRetLoc(cnt)+ ""));

    		if (localST == null || !localST.isComplete(id))
    		{
    			localST = globalSymbolTable;
    			sematicStack.push(new TempVar(AddressingMode.GLOBAL_DIRECT, localST.getTypeDesc(id), "" + localST.getLocation(id)));

    		}
    		else
    		{
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, localST.getTypeDesc(id), "" + localST.getLocation(id)));
    		}
    		
    		codeGenerate("singleRHSassign", token);
    		returnCounter.push(cnt+1);
    		break;
    	case "returnEnd":
    		returnCounter.pop();
    		codeGenerate("functionEnd", token);
    		break;
    	default:
    		return false;
    	}
    	return true;
    }
    
    
    
    SingleTypeDesc curStruct = null;
    private boolean structCg(String sem, Token token) throws Exception {
    	switch (sem) {
    	case "structName":
    		curStruct = new SingleTypeDesc(new SymbolTable(0));
    		types.put(idStack.pop(), curStruct);
    		break;
    	
    	case "structBlockBegin":
    		break;
    		
    	case "structVar":
    		curStruct.getStructDesc().addVar(idStack.pop(), getTypeDescForName(idStack.pop()));
    		break;
  
    	default:
    		return false;
    	}
    	return true;
    }
    
    Stack<TempVar> callArg = new Stack<>();
    Stack<FunctionDesc> callFunc = new Stack<>(); 
	private boolean callCg(String sem, Token token) throws Exception {
		FunctionDesc func;
		int loc;
    	switch (sem) {

    	case "callBeginX":
    		codeGenerate("idIdxEnd", token);
    		codeGenerate("callBegin", token);
    		break;
    		
    	case "callBegin":
    		func = functionTable.getFunction(idStack.pop());
    		callFunc.add(func);
    		callArg.clear();
    		break;
    		
    	case "callArg":
    		TempVar tmp = sematicStack.pop();
    		callArg.add(tmp);
    		if(callFunc.peek().getArgTypeDesc().get(callArg.size()-1) != tmp.getTypeDesc())
    			exceptionHandler(new  CodeError("invalid arg type", scanner.lineNumber() + ""));

    		break;
    	
    	case "callEnd":
    		func = callFunc.pop();
    		if(callArg.size() != func.getArgName().size())
    			exceptionHandler(new  CodeError("missmatch arg length", scanner.lineNumber() + ""));
    		
    		symbolTableStack.peek().addBubble(func.getRetSize());
    		
    		for(TempVar i: callArg){
        		TempVar arg = getTempVar("function_arg", i);
        		instructions.add(new Simple(":=", i.toOperand(), arg.toOperand()));
    		}
    		
    		TempVar sp = getTempVar("sp", getTypeDescForName("int"));
    		TempVar pc = getTempVar("pc", getTypeDescForName("int"));
    		instructions.add(new Simple(":=sp", sp.toOperand()));
    		instructions.add(new Simple(":=pc", pc.toOperand()));
    		instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 4+"") ,pc.toOperand() ,pc.toOperand()));
    		instructions.add(new Simple("+sp", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, symbolTableStack.peek().getCurrentEnd() + "")));
    		instructions.add(func.get_jumpTo());
    		instructions.add(new Simple("sp:=", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, -2 + "")));
    		symbolTableStack.peek().addBubble(-2 - func.getArgSize());
    		break;
    	
    	case "callAddRet":
    		func = callFunc.peek();
    		codeGenerate("callEnd", token);
    		loc = symbolTableStack.peek().getCurrentEnd() - func.getRetSize();
    		int cnt=0;
    		for(TypeDesc type: func.getRetType()){
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, type, loc + ""));
    			loc += type.getSize();
    			cnt++;
    			if(cnt > 1){
    				exceptionHandler(new  CodeError("multi return function cant use in expr", scanner.lineNumber() + ""));
    			}
    		}
    		break;
    		
    	case "callAssignRet":
    		func = callFunc.peek();
    		codeGenerate("callEnd", token);
    		loc = symbolTableStack.peek().getCurrentEnd() - func.getRetSize();
    		for(TypeDesc type: func.getRetType()){
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, type, loc + ""));
    			loc += type.getSize();
    			codeGenerate("multi_assign_add_exp", token);
    		}
    		codeGenerate("multi_assign_end", token);
    		break;
    	default:
    		return false;
    	}
    	return true;
    }
    
    private boolean expCg(String sem, Token token) throws Exception {
    	TempVar x, y, res;
    	String op;
    	
    	switch (sem) {
    	case "expInt":
    		id_name_stack.push(false);
    		sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.INTEGER.getSingleTypeDesc() , token.getText()));
    		break;
    		
    	case "expStr":
    		id_name_stack.push(false);
    		x = getTempVar(sem, BasicType.STRING.getSingleTypeDesc());
    		TempVar cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
    		instructions.add(new Simple("gmm", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, token.getText().length() + 1 + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, x.getValue() + "")));
    		instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, x.getValue() + ""), cnt.toOperand()));
    		for(int i=0; i<token.getText().length(); i++){
    			String tmp = (int)(token.getText().charAt(i)) + "";
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , cnt.toOperand(), cnt.toOperand()));
    			instructions.add(new Simple(":=", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, tmp + ""), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue())));
    		}
    		instructions.add(new Simple(":=", cnt.toOperand(), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, x.getValue() + "")));
    		sematicStack.push(x);
    		break;
    		
    	case "expReal":
    		id_name_stack.push(false);
    		sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.REAL.getSingleTypeDesc(), token.getText()));
    		break;
    	
    	case "expChar":
    		id_name_stack.push(false);
    		sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.CHARACTER.getSingleTypeDesc(), token.getText()));
    		break;
    	
    	case "expBool":
    		id_name_stack.push(false);
    		sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.BOOL.getSingleTypeDesc(), token.getText()));
    		break;
    		
    		
    	case "expId":
    		codeGenerate("idName", token);
    		break;
    		
    	case "exp1End":
    		codeGenerate("idIdEnd", token);
    		break;
    		
    	case "expIdxEnd":
    		codeGenerate("idIdxEnd", token);
    		break;
    	
    	case "expIsvoid":
    	case "expPar":
    		id_name_stack.push(false);
    		break;
    	
    	case "expLOr":
    	case "expLAnd":
    		op = sem.equals("expLOr") ? "||" : "&&";
    		
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureBoolean(y, op);
    		ensureBoolean(x, op);
    		
    		res = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expEq":
    	case "expNEq":
    		op = sem.equals("expEq") ? "==" : "!=";
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		
    		if (x.getTypeDesc() != y.getTypeDesc()) // TODO check for equality
    			exceptionHandler(new  CodeError(op + " is only supported for same types (used for " + x.getTypeDesc() + " and " + y.getTypeDesc() + ")", "on line " + scanner.lineNumber()));
    		ensureIntOrFloatOrBoolean(x, op);
    		
    		res = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expGt":
    	case "expGte":
    	case "expLt":
    	case "expLte":
    		if (sem.equals("expGt"))
    			op = ">";
    		else if (sem.equals("expGte"))
    			op = ">=";
    		else if (sem.equals("expLt"))
    			op = "<";
    		else
    			op = "<=";
    		
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureIntOrFloatOrBoolean(y, op);
    		ensureIntOrFloatOrBoolean(x, op);

    		res = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expPlus":
    	case "expMinus":
    		op = sem.equals("expPlus") ? "+" : "-";

    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureIntOrFloat(y, op);
    		ensureIntOrFloat(x, op);
    		res = getTempVar(sem, x, y);
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;

    	case "expBXor":
    	case "expBOr":
    	case "expBAnd":
    		if(sem.equals("expBXor"))
    			op = "^";
    		else if(sem.equals("expBOr"))
    			op = "|";
    		else
    			op = "&";
    		
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureIntOrFloat(y, op);
    		ensureIntOrFloat(x, op);
    		
    		res = getTempVar(sem, x, y);
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expMul":
    	case "expDiv":
    		op = sem.equals("expMul") ? "*" : "/";
    		
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureIntOrFloat(y, op);
    		ensureIntOrFloat(x, op);
    		
    		res = getTempVar(sem, x, y);
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    	
    	case "expMod":
    		op = "%";
    		
    		y = sematicStack.pop();
    		x = sematicStack.pop();
    		ensureIntOrFloat(y, op);
    		ensureIntOrFloat(x, op);
    		
    		res = getTempVar(sem, x, y);
    		instructions.add(new Simple(op, x.toOperand(), y.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expLNot":
    		x = sematicStack.pop();
    		ensureBoolean(x, "!");
    		res = getTempVar(sem, x);
    		instructions.add(new Simple("!", x.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expUMinus":
    		x = sematicStack.pop();
    		ensureIntOrFloat(x, "-");
    		res = getTempVar(sem, x);
    		instructions.add(new Simple("u-", x.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	case "expBNot":
    		x = sematicStack.pop();
    		ensureIntOrFloat(x, "~");
    		res = getTempVar(sem, x);
    		instructions.add(new Simple("~", x.toOperand(), res.toOperand()));
    		sematicStack.push(res);
    		break;
    		
    	default:
    		return false;
    	}
    	return true;
    }	
	
    private String simpleVarName;
	private boolean varDeclCg(String sem, Token token) throws Exception {
		String varName;
		TypeDesc type;
		int location;
		
    	switch (sem) {
    	case "arrayHasRHS":
//    		idStack.push(simpleVarName);
    		sematicStack.push(new TempVar(null, null, null));
			arrayRHSName = simpleVarName;
    		break;
    	case "simpleVarName":
    		varName = idStack.pop();
    		simpleVarName = varName;
    		type = getTypeDescForName(idStack.pop());
    		if(!symbolTableStack.isEmpty())
    		{
	    		if (symbolTableStack.peek().hasVar(varName))
	    			exceptionHandler(new  CodeError("Duplicate identifier", "on line " + scanner.lineNumber()));
	    		location = symbolTableStack.peek().addVar(varName, type, false);
	    		sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, type, "" + location));
    		}else{
        		if (globalSymbolTable.hasVar(varName))
        			exceptionHandler(new  CodeError("Duplicate identifier", "on line " + scanner.lineNumber()));
        		location = globalSymbolTable.addVar(varName, type, false);
        		sematicStack.push(new TempVar(AddressingMode.GLOBAL_DIRECT, type, "" + location));    			
    		}
       		break;
       	
    	case "arrayVarName":
    		varName = idStack.pop();
    		simpleVarName = varName;
    		type = getTypeDescForName(idStack.pop());
    		idStack.push(varName);
    		type = new ArrayTypeDesc(new ArrayList<TempVar>(), (SingleTypeDesc) type);
    		if(!symbolTableStack.isEmpty())
    		{
	    		if (symbolTableStack.peek().hasVar(varName))
	    			exceptionHandler(new  CodeError("Duplicate identifier", "on line " + scanner.lineNumber()));
	    		symbolTableStack.peek().addVar(varName, type, false);
    		}else{
	    		if (globalSymbolTable.hasVar(varName))
	    			exceptionHandler(new  CodeError("Duplicate identifier", "on line " + scanner.lineNumber()));
	    		globalSymbolTable.addVar(varName, type, false);
    		}
    		break;
    	case "arrayVarEnd":
    		if(!symbolTableStack.isEmpty())
    			symbolTableStack.peek().setComplete(simpleVarName);
    		else
    			globalSymbolTable.setComplete(simpleVarName);

    		idStack.pop();
    		break;
    		
    	case "simpleVarEnd":
    		if(!symbolTableStack.isEmpty())
    			symbolTableStack.peek().setComplete(simpleVarName);
    		else
    			globalSymbolTable.setComplete(simpleVarName);
    		sematicStack.clear();
    		break;
    	default:
    		return false;
    	}
    	return true;
	}
	
	boolean inFunctionBlock;
	private boolean blockCg(String sem) throws Exception {
		SymbolTable st;
    	switch (sem) {
    	case "blockBegin":
    		int beg = symbolTableStack.isEmpty()? 0 : symbolTableStack.peek().getCurrentEnd();
    		if(inFunctionBlock){
    			beg = -functionTable.getFunction(functionName).getArgSize() - 1 - 1; //top, pc, sp, arg2, arg1, ret2, ret1
    		}
    		st = new SymbolTable(beg);
    		symbolTableStack.push(st);
    		if(inFunctionBlock){
    			FunctionDesc func = functionTable.getFunction(functionName);
    			for(int i=0; i < func.getArgName().size(); i++){
    				st.addVar(func.getArgName().get(i), func.getArgTypeDesc().get(i));
    			}
    			st.setLastLocation(0);
    			inFunctionBlock = false;
    		}
    		break;
    	
    	case "blockEnd":
    		st = symbolTableStack.pop();
    		st.close();
    		break;
    		
    	default:
    		return false;
    	}
    	return true;
    }
	
	private boolean ioCg(String sem, Token token) throws Exception {
		TempVar x;
		
    	switch (sem) {
    	
//    	case "writeStr":
//    	case "writeChar":
//		instructions.add(new Simple("wt", new Operand(AddressingMode.IMMEDIATE, OperandType.STRING, CharacterClass.escape(token.getText()))));
//    		break;
    	
    	case "readEnd":
    		// TODO read text
    		x = sematicStack.pop();
    		if(x.getTypeDesc() == getTypeDescForName("int"))
    			instructions.add(new Simple("ri", x.toOperand()));
    		else if(x.getTypeDesc() == getTypeDescForName("real"))
    			instructions.add(new Simple("rf", x.toOperand()));
    		else if(x.getTypeDesc() == getTypeDescForName("char"))
    			instructions.add(new Simple("rt", x.toOperand()));
    		else if(x.getTypeDesc() == getTypeDescForName("string"))
    		{
    			TempVar cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
        		instructions.add(new Simple("gmm", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1000 + ""), new Operand(x.getAddressMode(), OperandType.INTEGER, x.getValue() + "")));
        		instructions.add(new Simple(":=", new Operand(x.getAddressMode(), OperandType.INTEGER, x.getValue() + ""), cnt.toOperand()));
        		TempVar cond = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") ,cnt.toOperand(), cnt.toOperand()));
        		instructions.add(new Simple("rt", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.CHAR, cnt.getValue())));
    			instructions.add(new Simple("==", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 10 + ""), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue()), cond.toOperand()));
    			instructions.add(new JumpRelative(cond.toOperand(), -3));
    			TempVar tmp = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
    			instructions.add(new Simple(":=", new Operand(x.getAddressMode(), OperandType.INTEGER, x.getValue()), tmp.toOperand()));
    			instructions.add(new Simple(":=", cnt.toOperand(), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, tmp.getValue())));
    		}
    		else
    			exceptionHandler(new  CodeError("read only real, int, string, char supported", scanner.lineNumber() + ""));
    		break;
    	
    	case "writeEnd":
    		// TODO write text
    		x = sematicStack.pop();
    		if(x.getTypeDesc() == getTypeDescForName("int"))
    			instructions.add(new Simple("wi", x.toOperand()));
    		else if(x.getTypeDesc() == getTypeDescForName("real"))
    			instructions.add(new Simple("wf", x.toOperand()));
    		else if(x.getTypeDesc() == getTypeDescForName("char"))
    		{
    			if(x.getAddressMode() == AddressingMode.IMMEDIATE)
    				instructions.add(new Simple("wt", new Operand(x.getAddressMode(), OperandType.pert, x.getValue())));
    			else
    				instructions.add(new Simple("wt", x.toOperand()));
    		}
    		else if(x.getTypeDesc() == getTypeDescForName("bool"))
    		{
    			String val = x.getValue();
    			if(x.getAddressMode() == AddressingMode.IMMEDIATE)
					val = x.getValue().equals("false")? "0" : "1";
    			instructions.add(new Simple("wi", new Operand(x.getAddressMode(), OperandType.INTEGER, val)));
    		}
    		else if(x.getTypeDesc() == getTypeDescForName("string")){
    			TempVar cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
        		TempVar cond = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
        		TempVar size = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
        		instructions.add(new Simple(":=", new Operand(x.getAddressMode(), OperandType.INTEGER, x.getValue() + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, cnt.getValue() + "")));
        		instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue() + ""), size.toOperand()));
    			instructions.add(new Simple("!=", size.toOperand(), cnt.toOperand(), cond.toOperand()));
    			instructions.add(new JumpRelative(cond.toOperand(), 4));
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , cnt.toOperand(), cnt.toOperand()));
    			instructions.add(new Simple("wt", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.CHAR, cnt.getValue() + "")));
    			instructions.add(new JumpRelative(null, -4));
    		}
    		else
    			exceptionHandler(new  CodeError("write only real, int, char, string, bool supported", scanner.lineNumber() + ""));
    		break;
    	
    	default:
    		return false;
    	}
    	return true;
    }
	
    Stack<Integer> arrayDimCounter = new Stack<>();
	Stack<Boolean> locationType = new Stack<>(); // true is indexing
	Stack<Boolean> id_name_stack = new Stack<>();
	private boolean idCg(String sem) throws Exception {
		String varName;
		SymbolTable localST;
//		System.out.println("ss " + sematicStack);
//		System.out.println("ids " + idStack);
//		System.out.println("loctype " + locationType);
//		System.out.println("indexcount " + indexCount);
//		System.out.println();
		
    	switch (sem) {

    	case "idIdEnd":
    		if(id_name_stack.pop()){
	    		String id = idStack.pop();
	    		
	    		localST = findInSymbolTable(id);
	    		if (localST != null) {
	    			if(!localST.isComplete(id))
		    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
	    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, localST.getTypeDesc(id), "" + localST.getLocation(id)));
	    		} else if (globalSymbolTable.hasVar(id)) {
	    			if(!globalSymbolTable.isComplete(id))
		    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
	    			sematicStack.push(new TempVar(AddressingMode.GLOBAL_DIRECT, globalSymbolTable.getTypeDesc(id), "" + globalSymbolTable.getLocation(id)));
	    		} else
	    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
    		}
    		break;
    	
    	case "idName":
    		id_name_stack.push(true);
    		break;
    	
    	case "idIdxEnd":
    		id_name_stack.pop();
    		id_name_stack.push(false);
    		break;

    	case "idIndexBeign":
    		arrayDimCounter.push(0);
    		locationType.push(sem.equals("idIndexBeign"));
    		varName = idStack.pop();
    		localST = findInSymbolTable(varName);
    		if (localST != null) {
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_INDIRECT, localST.getTypeDesc(varName), "" + localST.getLocation(varName)));
    		} else if (globalSymbolTable.hasVar(varName)) {
    			sematicStack.push(new TempVar(AddressingMode.GLOBAL_INDIRECT, globalSymbolTable.getTypeDesc(varName), "" + globalSymbolTable.getLocation(varName)));
    		} else
    			exceptionHandler(new  CodeError("Unknown variable identifier: " + varName, "on line " + scanner.lineNumber()));
    		break;
    	
    	case "idFieldBegin":
    		locationType.push(sem.equals("idIndexBeign"));
    		varName = idStack.pop();
    		localST = findInSymbolTable(varName);
    		if (localST != null) {
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, localST.getTypeDesc(varName), "" + localST.getLocation(varName)));
    		} else if (globalSymbolTable.hasVar(varName)) {
    			sematicStack.push(new TempVar(AddressingMode.GLOBAL_DIRECT, globalSymbolTable.getTypeDesc(varName), "" + globalSymbolTable.getLocation(varName)));
    		} else
    			exceptionHandler(new  CodeError("Unknown variable identifier: " + varName, "on line " + scanner.lineNumber()));
    		break;
    	case "idIndexAfterField":
    		arrayDimCounter.push(0);
    		break;
    		
    	case "idIndex":
    		TempVar tempVar;
    		int dimNum = arrayDimCounter.pop();
    		if(dimNum == 0){
	    		// semantic stack after execution: arrayRHS, firstIndexExp, <top>
	    		TempVar firstIndex = sematicStack.pop();
	    		ensureInt(firstIndex, "[]");
	    		if (!sematicStack.peek().getTypeDesc().isArray())
	    			exceptionHandler(new  CodeError("Indexing non-array type: " + sematicStack.peek().getTypeDesc(), "on line " + scanner.lineNumber()));
	    		tempVar = getTempVar("arrayindex", BasicType.INTEGER.getSingleTypeDesc());
	    		instructions.add(new Simple(":=", firstIndex.toOperand(), tempVar.toOperand()));
    		}else{
	    		// ss before: array, prevIndex, curIndexExp, <top>
	    		// ss after: array, index, <top>
	    		TempVar indexVal = sematicStack.pop();
	    		TempVar prevVal = sematicStack.pop();
	    		TempVar array = sematicStack.peek();
	    		ensureInt(indexVal, "[]");
	    		ArrayTypeDesc arrayType = (ArrayTypeDesc)array.getTypeDesc();
	    		
	    		if (dimNum == arrayType.getArrayDimensions().size())
	    			exceptionHandler(new  CodeError("Array indexed more than its dimensions", "on line " + scanner.lineNumber()));
	    		tempVar = prevVal;
	    		instructions.add(new Simple("*", prevVal.toOperand(), arrayType.getArrayDimensions().get(dimNum).toOperand(), tempVar.toOperand()));
	    		instructions.add(new Simple("+", tempVar.toOperand(), indexVal.toOperand(), tempVar.toOperand()));
    		}
    		sematicStack.push(tempVar);
    		arrayDimCounter.push(dimNum + 1);
    		break;
    		
    	case "idIndexFinish":
    		if (locationType.pop() == true) { // index
    			TempVar indexVal = sematicStack.pop();
        		TempVar array = sematicStack.pop();
        		ArrayTypeDesc arrayType = (ArrayTypeDesc)array.getTypeDesc();
        		SingleTypeDesc underlyingType = arrayType.getUnderlyingType();
        		
        		int dims = arrayDimCounter.pop();
        		if (dims != arrayType.getArrayDimensions().size())
        			exceptionHandler(new  CodeError("Array has " + arrayType.getArrayDimensions().size() + " dimensions not " + dims, "on line " + scanner.lineNumber()));
        			
        		// *= size(underlying)
        		instructions.add(new Simple("*", indexVal.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + underlyingType.getSize()), indexVal.toOperand()));
        		// += address(array)
        		if (array.getAddressMode().isGlobal())
        			instructions.add(new Simple("+", indexVal.toOperand(), new Operand(AddressingMode.GLOBAL_DIRECT, OperandType.INTEGER, "" + array.getValue()), indexVal.toOperand()));
        		else
        			instructions.add(new Simple("+", indexVal.toOperand(), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, "" + array.getValue()), indexVal.toOperand()));
        		// something with LI or GI address in ss
        		sematicStack.push(new TempVar(array.getAddressMode(), underlyingType, indexVal.getValue()));
    		} 
//    		else {
//    			TempVar address = sematicStack.pop();
//    			TempVar struct = sematicStack.pop();
//    			sematicStack.push(new TempVar(struct.getAddressMode(), address.getTypeDesc(), address.getValue()));
//    		}
    		break;
//    	case "locationDot2":
////    		if (locationType.peek() == true)
//    			idCg("locationEnd");
//    			locationType.push(false);
//    		break;
//    	
//    	case "locationIndexType2":
//    		idCg("locationEnd");
//    		locationType.push(true);
//    		break;
//    		
    	case "idField":
    	case "idFirstField":
    		String fieldName = idStack.pop();
    		
    		TempVar struct = sematicStack.pop();
    		if (struct.getTypeDesc().isArray())
    			exceptionHandler(new  CodeError(". is for structs not array, first index it", "on line " + scanner.lineNumber()));
    		SingleTypeDesc s = (SingleTypeDesc)struct.getTypeDesc();
    		if (!s.isStruct())
    			exceptionHandler(new  CodeError(". is for structs not " + s.toString(), "on line " + scanner.lineNumber()));
    		
    		SymbolTable structDesc = s.getStructDesc();
    		if (!structDesc.hasVar(fieldName))
    			exceptionHandler(new  CodeError("field not found: " + fieldName, "on line " + scanner.lineNumber()));
    		
    		Operand rel = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + structDesc.getLocation(fieldName));

    		TypeDesc typeDesc = structDesc.getTypeDesc(fieldName);
    		System.out.println(fieldName);
    		System.out.println(typeDesc.toString());
//    		System.out.println("SSS " + structDesc + " " + typeDesc);
			Operand op1 = new Operand(struct.getAddressMode(), OperandType.INTEGER, "" + struct.getValue());
			TempVar tmp = getTempVar("dot", typeDesc);
			Operand op2 = new Operand(tmp.getAddressMode(), OperandType.INTEGER, "" + tmp.getValue());
			instructions.add(new Simple("+", rel, op1, op2));
			sematicStack.push(new TempVar(AddressingMode.LOCAL_INDIRECT, tmp.getTypeDesc(), tmp.getValue()));
    		
    		break;
    	default:
    		return false;
    	}
    	return true;
    }
	
	private String arrayRHSName;
	private Stack<Integer> lastMultiRhs = new Stack<>();
	boolean isMultiRhsNull = true;
	private boolean assignCg(String sem) throws Exception {
		
    	SymbolTable localST;
    	ArrayTypeDesc arrayRHSTypeDesc;
    	SymbolTable structDesc;
    	SingleTypeDesc s;
    	TempVar struct;
    	TempVar cnt;
    	int last, next_size;
		switch (sem) {
		case "simpleVarAssign":
    		String id = idStack.pop();
    		
    		localST = findInSymbolTable(id);
    		if (localST != null) {
    			sematicStack.push(new TempVar(AddressingMode.LOCAL_DIRECT, localST.getTypeDesc(id), "" + localST.getLocation(id)));
    		} else if (globalSymbolTable.hasVar(id)) {
    			sematicStack.push(new TempVar(AddressingMode.GLOBAL_DIRECT, globalSymbolTable.getTypeDesc(id), "" + globalSymbolTable.getLocation(id)));
    		} else
    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
    		arrayRHSName = id;
			break;

		case "singleRHSassign":
    		TempVar x = sematicStack.pop(); 
    		TempVar loc = sematicStack.pop();
    		
    		if (x.getTypeDesc() != loc.getTypeDesc()) //TODO
    			exceptionHandler(new  CodeError("Sides of assignment must be same type (are " + loc.getTypeDesc() + " and " + x.getTypeDesc() + ")", "on line " + scanner.lineNumber()));
    		
    			instructions.add(new Simple(":=", x.toOperand(), loc.toOperand()));
			break;
		case "arrayBeginArg":
			sematicStack.pop();
//			String varName = idStack.pop();
//			arrayRHSName = varName;
//			arrayRHSTypeDesc = (ArrayTypeDesc) findInSymbolTable(varName).getTypeDesc(varName);
//			arrayRHSTypeDesc.resetDim();
			try{
				((ArrayTypeDesc) findInSymbolTable(arrayRHSName).getTypeDesc(arrayRHSName)).resetDim();
			} 
			catch(Exception exp){
				((ArrayTypeDesc)globalSymbolTable.getTypeDesc(arrayRHSName)).resetDim();
			}
			break;
		case "arrayAddArg":
			TempVar tmp = sematicStack.pop();
			TempVar dim = getTempVar(sem, tmp);
			try{
				arrayRHSTypeDesc = ((ArrayTypeDesc) findInSymbolTable(arrayRHSName).getTypeDesc(arrayRHSName));
				dim = new TempVar(AddressingMode.LOCAL_DIRECT, 
						BasicType.INTEGER.getSingleTypeDesc(), 
						(findInSymbolTable(arrayRHSName).getLocation(arrayRHSName) + arrayRHSTypeDesc.getArrayDimensions().size() * 1 + 1) + "");
			} 
			catch(Exception exp){
				arrayRHSTypeDesc = ((ArrayTypeDesc)globalSymbolTable.getTypeDesc(arrayRHSName));
				dim = new TempVar(AddressingMode.GLOBAL_DIRECT, 
						BasicType.INTEGER.getSingleTypeDesc(), 
						(globalSymbolTable.getLocation(arrayRHSName) + arrayRHSTypeDesc.getArrayDimensions().size() * 1 + 1) + "");
			}
			instructions.add(new Simple(":=", tmp.toOperand(), dim.toOperand()));
    		arrayRHSTypeDesc.addDim(dim);
    		break;
		case "arrayEndArg":
			int location;
			TempVar size;
			try{
				location = findInSymbolTable(arrayRHSName).getLocation(arrayRHSName);
				arrayRHSTypeDesc = ((ArrayTypeDesc) findInSymbolTable(arrayRHSName).getTypeDesc(arrayRHSName));
				size = new TempVar(AddressingMode.LOCAL_DIRECT, 
						BasicType.INTEGER.getSingleTypeDesc(), 
						(location + arrayRHSTypeDesc.getArrayDimensions().size() + 1) + "");
			} 
			catch(Exception exp){
				location = globalSymbolTable.getLocation(arrayRHSName);
				arrayRHSTypeDesc = ((ArrayTypeDesc)globalSymbolTable.getTypeDesc(arrayRHSName));
				size = new TempVar(AddressingMode.GLOBAL_DIRECT, 
						BasicType.INTEGER.getSingleTypeDesc(), 
						(location + arrayRHSTypeDesc.getArrayDimensions().size() + 1) + "");
			}
//			System.out.println("slam");
			instructions.add(new MakeArray(arrayRHSTypeDesc, size, location));
			break;
			
			
    	case "multiRhsBegin":
    		struct = sematicStack.peek();
    		if (struct.getTypeDesc().isArray())
    			exceptionHandler(new  CodeError("multi assign is for structs not array, first index it", "on line " + scanner.lineNumber()));
    		s = (SingleTypeDesc)struct.getTypeDesc();
    		if (!s.isStruct())
    			exceptionHandler(new  CodeError("multi assign is for structs not " + s.toString(), "on line " + scanner.lineNumber()));
    		
    		structDesc = s.getStructDesc();
    		Operand op1 = new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, "" + structDesc.getSize());
    		Operand op2 = new Operand(struct.getAddressMode(), OperandType.INTEGER, "" + struct.getValue());
			instructions.add(new Simple("gmm", op1, op2));
			cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
			instructions.add(new Simple(":=", op2 ,cnt.toOperand()));
			sematicStack.push(cnt);
			lastMultiRhs.push(0);
    		break;
    	case "multiRhsEnd":
    		sematicStack.pop();
    		sematicStack.pop();
    		lastMultiRhs.pop();
    		break;
    	case "multiRhsXEnd":
			last = lastMultiRhs.pop();
			TempVar exp = null;
			if(isMultiRhsNull == false)
				exp = sematicStack.pop();
			cnt = sematicStack.pop();
    		struct = sematicStack.peek();
    		s = (SingleTypeDesc)struct.getTypeDesc();
    		structDesc = s.getStructDesc();
    		next_size = structDesc.getNextSize(last);
			lastMultiRhs.push(last + next_size);
			if(isMultiRhsNull == false)
			{
				if(!exp.getTypeDesc().isArray() && ((SingleTypeDesc)exp.getTypeDesc()).isStruct())
					instructions.add(new Simple(":=", new Operand(exp.getAddressMode(), OperandType.INTEGER, exp.getValue()), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue())));
				else
					instructions.add(new Simple(":=", exp.toOperand(), new Operand(AddressingMode.LOCAL_INDIRECT, exp.toOperand().getVarType(), cnt.getValue())));
			}
			instructions.add(new Simple("+", cnt.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, next_size+""), cnt.toOperand()));
			sematicStack.push(cnt);
			isMultiRhsNull = true;
    		break;
    	case "multiRhsExp":
    		isMultiRhsNull = false;
    		break;
    	
    	case "multiRhsEXPXEnd":
    		assignCg("multiRhsExp");
    		assignCg("multiRhsXEnd");
    		break;
    	default:
    		return false;
    	}
    	return true;
    }
	
	
	Queue<TempVar> multi_assign = new LinkedList<>();
	private boolean multi_assignCg(String sem) throws Exception {
		
		TempVar x;
		switch (sem) {
		case "multi_assign_add_id":
    		x = sematicStack.pop(); 
    		multi_assign.add(x);
			break;

		case "multi_assign_add_exp":
    		x = sematicStack.pop();
    		TempVar id = multi_assign.poll();
    		if(id == null){
				exceptionHandler(new  CodeError("too much exp rhs of assignment!", scanner.lineNumber() + "")); 
    		}
    		if(id.getTypeDesc() != x.getTypeDesc())
    			exceptionHandler(new  CodeError("return type mismatched", scanner.lineNumber() + ""));
//    		if(x.getTypeDesc().toString().equals("STRING")){
//    			instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, Integer.parseInt(x.getValue()) + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, id.getValue() + "")));
//    			instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, Integer.parseInt(x.getValue()) + 1 + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, Integer.parseInt(id.getValue()) + 1 + "")));
//    		}else{
    			instructions.add(new Simple(":=", x.toOperand(), id.toOperand()));
//    		}
			break;
		
		case "multi_assign_end":
			if(multi_assign.size() != 0){
				exceptionHandler(new  CodeError("expected another exp after assignment!", scanner.lineNumber() + ""));
			}
			break;
    	default:
    		return false;
    	}
    	return true;
    }
	
	Stack<Dummy> ifStack = new Stack<>();	
	private boolean ifCg(String sem) throws Exception {
		Dummy dummy;
		
    	switch (sem) {
    	case "ifBegin":
    		TempVar cond = sematicStack.pop();
    		ensureBoolean(cond, "ifCondition");
    		
    		dummy = new Dummy();
    		ifStack.push(dummy);
    		TempVar newCond = getTempVar(sem, cond);
    		instructions.add(new Simple("!", cond.toOperand(), newCond.toOperand())); //**king jz
    		instructions.add(new JumpRelative(newCond.toOperand(), 2));
    		instructions.add(new Jump(dummy, null));
    		break;
    		
    	case "ifEndMainBlock":
    		instructions.add(ifStack.pop());
    		break;
    		
    	case "ifElse":
    		MetaInstruction d = instructions.remove(instructions.size() - 1);
    		
    		dummy = new Dummy();
    		ifStack.push(dummy);
    		
    		instructions.add(new Jump(dummy, null));
    		instructions.add(d);
    		break;
    		
    	case "ifEndElseBlock":
    		instructions.add(ifStack.pop());
    		break;
    		
    	default:
    		return false;
    	}
    	return true;
    }
	
	Stack<MetaInstruction> doStack = new Stack<>();
	Stack<MetaInstruction> whileStack = new Stack<>();
	Stack<MetaInstruction> beginLoop = new Stack<>();
	Stack<MetaInstruction> endLoop = new Stack<>();
	private boolean doWhileCg(String sem) throws Exception {
		Dummy dummy;
		TempVar cond;
		
    	switch (sem) {
    	case "loopContinue":
    		instructions.add(new Jump(beginLoop.peek(), null));
    		break;
    	case "loopBreak":
    		instructions.add(new Jump(endLoop.peek(), null));
    		break;

    	case "doBeginBlock":
    		dummy = new Dummy();
    		doStack.add(dummy);
    		instructions.add(dummy);
    		
    		beginLoop.add(new Dummy());
    		endLoop.add(new Dummy());
    		break;
    	
    	case "doBeginExp":
    		instructions.add(beginLoop.peek());
    		break;
    	case "doEndExp":
    		cond = sematicStack.pop();
    		ensureBoolean(cond, "doCondition");
    		dummy = (Dummy) doStack.pop();
    		instructions.add(new JumpRelative(cond.toOperand(), 2));
    		instructions.add(new Jump(dummy, null));
    		
    		instructions.add(endLoop.pop());
    		beginLoop.pop();
    		break;
    		
    	case "whileBeginExp":
    		dummy = new Dummy();
    		whileStack.add(dummy);
    		instructions.add(dummy);
    		
    		beginLoop.add(dummy);
    		endLoop.add(new Dummy());
    		break;
    	
    	case "whileEndExp":
    		cond = sematicStack.pop();
    		ensureBoolean(cond, "whileCondition");
    		dummy = (Dummy) whileStack.pop();
    		MetaInstruction endBlock = new Jump(dummy, null);
    		whileStack.add(endBlock);
    		instructions.add(new Jump(endBlock, cond.toOperand(), true));
    		break;
    		
    	case "whileEndBlock":
    		instructions.add(whileStack.pop());
    		instructions.add(endLoop.pop());
    		beginLoop.pop();
    		break;
    		
    	default:
    		return false;
    	}
    	return true;
    }
	
	Stack<Case> caseStack = new Stack<>();
	private boolean caseCg(String sem, Token token) throws Exception {
		Dummy dummy;
		Case cas;
    	switch (sem) {
    	case "caseEndExp":
    		TempVar cond = sematicStack.pop();
    		ensureInt(cond, "caseCondition");
    		cas = new Case(cond, getTempVar(sem, cond), getTempVar(sem, BasicType.BOOL.getSingleTypeDesc()));
    		caseStack.push(cas);
//    		instructions.add(new Jump(dummy, null));
    		cas.addConditionPosition(instructions.size());
    		break;
    		
    	case "caseVal":
    		cas = caseStack.peek();
    		dummy = new Dummy();
    		cas.addCase(token.getText(), dummy);
    		instructions.add(dummy);
    		break;
    		
    	case "caseEndBlock":
    		cas = caseStack.peek();
    		instructions.add(new Jump(cas.getEndDummy(), null));
    		break;
    		
    	case "caseEnd":
    		cas = caseStack.pop();
    		instructions.addAll(cas.getConditionPosition(), cas.getJumpTable());
    		instructions.add(cas.getEndDummy());
    		break;
    		
    	default:
    		return false;
    	}
    	return true;
    }
	
	HashMap<String, MetaInstruction> labels = new HashMap<>();
	private boolean gotoCg(String sem) throws Exception{
		String id;
		MetaInstruction label;
		switch (sem) {
		case "gotoId":
    		id = idStack.pop();
    		label = labels.get(id);
    		if(label == null){
    			label = new Dummy();
    			labels.put(id, label);
    		}
    		instructions.add(new Jump(label, null));

			break;
		case "labelId":
    		id = idStack.pop();
    		label = labels.get(id);
    		if(label == null){
    			label = new Dummy();
    			labels.put(id, label);
    		}
    		instructions.add(label);
			break;

		default:
			return false;
		}
		return true;
	}
	
	
	private boolean releaseVoidCg(String sem, Token token) throws Exception{
		String id;
		SymbolTable localST;
		TypeDesc typeDesc = null;
		TempVar x;
		switch (sem) {
		case "releaseId":
    		id = idStack.pop();
    		localST = findInSymbolTable(id);
    		Operand arr = null;
    		if (localST != null) {
        		typeDesc = localST.getTypeDesc(id);
        		arr = new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, localST.getLocation(id) + "");
    		} else if (globalSymbolTable.hasVar(id)) {
    			typeDesc = globalSymbolTable.getTypeDesc(id);
        		arr = new Operand(AddressingMode.GLOBAL_DIRECT, OperandType.INTEGER, globalSymbolTable.getLocation(id) + "");
    		} else
    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
    		if(typeDesc.isArray()){
    			TempVar size = ((ArrayTypeDesc) typeDesc).getTempVarSize();
    			if(size == null)
    				exceptionHandler(new  CodeError("array not assigend: ", "" + scanner.lineNumber()));
				instructions.add(new Simple("fmm" , arr ,size.toOperand()));
				instructions.add(new Simple(":=", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0+""), size.toOperand()));
    		}else if(((SingleTypeDesc)typeDesc).isStruct())
    		{
    			instructions.add(new Simple("fmm", arr, new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER ,""+((SingleTypeDesc)typeDesc).getStructDesc().getSize())));
    		}else
    			exceptionHandler(new  CodeError("only array and struct can be released", scanner.lineNumber() + ""));
			break;
		case "isVoid":
			//TODO
    		id = idStack.pop();
//    		sematicStack.pop();
    		localST = findInSymbolTable(id);
    		if (localST != null) {
        		typeDesc = localST.getTypeDesc(id);
    		} else if (globalSymbolTable.hasVar(id)) {
    			typeDesc = globalSymbolTable.getTypeDesc(id);
    		} else
    			exceptionHandler(new  CodeError("Unknown variable identifier: " + id, "on line " + scanner.lineNumber()));
    		if(typeDesc.isArray()){
    			TempVar size = ((ArrayTypeDesc) typeDesc).getTempVarSize();
    			if(size == null){
    				sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.BOOL.getSingleTypeDesc(), "false"));
    			}else{
    				TempVar tmp = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
    				instructions.add(new Simple("!=", size.toOperand(), new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 0+""), tmp.toOperand()));
    				sematicStack.push(tmp);
    			}
    		}
    		else if(((SingleTypeDesc)typeDesc).isStruct())
    			sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.BOOL.getSingleTypeDesc(), "true"));
    		else
				sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.BOOL.getSingleTypeDesc(), "false"));
			break;
			
		case "strlenExp":
    		id_name_stack.push(false);
			x = sematicStack.pop();
			if(x.getTypeDesc().toString().equals("STRING")){
				TempVar last = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
				instructions.add(new Simple(":=", x.toOperand() ,last.toOperand()));
				sematicStack.push(new TempVar(AddressingMode.LOCAL_INDIRECT, BasicType.INTEGER.getSingleTypeDesc(), last.getValue()));
				sematicStack.push(new TempVar(x.getAddressMode(), BasicType.INTEGER.getSingleTypeDesc(), x.getValue()));
				codeGenerate("expMinus", token);
			}else
				exceptionHandler(new  CodeError("strlen only works on string!", scanner.lineNumber() + ""));
			break;
			
		case "concatExp2":
    		id_name_stack.push(false);
			TempVar y = sematicStack.pop();
			x = sematicStack.pop();
			if(x.getTypeDesc().toString().equals("STRING") && y.getTypeDesc().toString().equals("STRING")){
				TempVar newString = getTempVar(sem, BasicType.STRING.getSingleTypeDesc());
				TempVar new_cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
    			TempVar cnt = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
        		TempVar cond = getTempVar(sem, BasicType.BOOL.getSingleTypeDesc());
        		sematicStack.push(x);
        		codeGenerate("strlenExp", token);
        		sematicStack.push(y);
        		codeGenerate("strlenExp", token);
        		codeGenerate("expPlus", token);
        		sematicStack.push(new TempVar(AddressingMode.IMMEDIATE, BasicType.INTEGER.getSingleTypeDesc(), "1"));
        		codeGenerate("expPlus", token);
        		
        		instructions.add(new Simple("gmm", sematicStack.pop().toOperand(), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, newString.getValue())));
        		instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, newString.getValue() + ""), new_cnt.toOperand()));

				TempVar last = getTempVar(sem, BasicType.INTEGER.getSingleTypeDesc());
				instructions.add(new Simple(":=", x.toOperand() ,last.toOperand()));
        		instructions.add(new Simple(":=", new Operand(x.getAddressMode(), OperandType.INTEGER, x.getValue() + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, cnt.getValue() + "")));
    			instructions.add(new Simple("!=", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, last.getValue()), cnt.toOperand(), cond.toOperand()));
    			instructions.add(new JumpRelative(cond.toOperand(), 5));
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , cnt.toOperand(), cnt.toOperand()));
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , new_cnt.toOperand(), new_cnt.toOperand()));
    			instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue() + ""), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER,new_cnt.getValue() )));
    			instructions.add(new JumpRelative(null, -5));
    			
				instructions.add(new Simple(":=", y.toOperand() ,last.toOperand()));
        		instructions.add(new Simple(":=", new Operand(y.getAddressMode(), OperandType.INTEGER, y.getValue() + ""), new Operand(AddressingMode.LOCAL_DIRECT, OperandType.INTEGER, cnt.getValue() + "")));
    			instructions.add(new Simple("!=", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, last.getValue()), cnt.toOperand(), cond.toOperand()));
    			instructions.add(new JumpRelative(cond.toOperand(), 5));
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , cnt.toOperand(), cnt.toOperand()));
    			instructions.add(new Simple("+", new Operand(AddressingMode.IMMEDIATE, OperandType.INTEGER, 1 + "") , new_cnt.toOperand(), new_cnt.toOperand()));
    			instructions.add(new Simple(":=", new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, cnt.getValue() + ""), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER,new_cnt.getValue() )));
    			instructions.add(new JumpRelative(null, -5));
    			
    			instructions.add(new Simple(":=", newString.toOperand(), last.toOperand()));
    			instructions.add(new Simple(":=", new_cnt.toOperand(), new Operand(AddressingMode.LOCAL_INDIRECT, OperandType.INTEGER, last.getValue())));
    			sematicStack.push(newString);
			}else
				exceptionHandler(new  CodeError("strlen only works on string!", scanner.lineNumber() + ""));
			break;
		default:
			return false;
		}
		return true;
	}
		
    /*private boolean expThings(String sem) throws Exception {
    	switch (sem) {
    	default:
    		return false;
    	}
    	return true;
    }*/	

	
    public void codeGenerate(String sem, Token token) throws Exception
    {
    	if (sem.charAt(0) == '@')
    		sem = sem.substring(1);
    	if (sem.equals("NoSem"))
    		return;

		System.err.println(String.format("CG %-5s %s","on line " + scanner.lineNumber(), sem));
		System.err.println(sematicStack);
//		System.out.println(idStack);
    	
    	if (!expCg(sem, token) &&
    		!varDeclCg(sem, token) &&
    		!blockCg(sem) &&
    		!ioCg(sem, token) &&
    		!idCg(sem) &&
    		!assignCg(sem) &&
    		!ifCg(sem) &&
    		!doWhileCg(sem) &&
    		!releaseVoidCg(sem, token) &&
    		!gotoCg(sem)&&
    		!multi_assignCg(sem) &&
    		!callCg(sem, token)&&
    		!caseCg(sem, token)&&
    		!structCg(sem, token)&&
    		!functionCg(sem, token)) {
    		System.err.println("Unknown semantics: " + sem);
    		exceptionHandler(new  RuntimeException("Unknown semantics: " + sem));
    	}
    }
    
    private void ensureNonArrayNonStruct(TempVar x, String operationStr) throws Exception {
		if (x.getTypeDesc().isArray())
			exceptionHandler(new  CodeError(operationStr + " is not supported for arrays", "on line " + scanner.lineNumber()));
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.isStruct())
			exceptionHandler(new  CodeError(operationStr + " is not supported for structs", "on line " + scanner.lineNumber()));
	}
	
	private void ensureIntOrFloat(TempVar x, String operationStr) throws Exception {
		ensureNonArrayNonStruct(x, operationStr);
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.getBasicType() != BasicType.INTEGER && d.getBasicType() != BasicType.REAL)
			exceptionHandler(new  CodeError(operationStr + " is not supported for " + d.getBasicType() + " (only int and real)", "on line " + scanner.lineNumber()));
	}
	
	private void ensureBoolean(TempVar x, String operationStr) throws Exception {
		ensureNonArrayNonStruct(x, operationStr);
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.getBasicType() != BasicType.BOOL)
			exceptionHandler(new  CodeError(operationStr + " is not supported for " + d.getBasicType() + " (only boolean)", "on line " + scanner.lineNumber()));
	}
	
	private void ensureInt(TempVar x, String operationStr) throws Exception {
		ensureNonArrayNonStruct(x, operationStr);
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.getBasicType() != BasicType.INTEGER)
			exceptionHandler(new  CodeError(operationStr + " is not supported for " + d.getBasicType() + " (only int)", "on line " + scanner.lineNumber()));
	}
	
	private void ensureFloat(TempVar x, String operationStr) throws Exception {
		ensureNonArrayNonStruct(x, operationStr);
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.getBasicType() != BasicType.REAL)
			exceptionHandler(new  CodeError(operationStr + " is not supported for " + d.getBasicType() + " (real)", "on line " + scanner.lineNumber()));
	}
	
	private void ensureIntOrFloatOrBoolean(TempVar x, String operationStr) throws Exception {
		ensureNonArrayNonStruct(x, operationStr);
		SingleTypeDesc d = (SingleTypeDesc) x.getTypeDesc();
		if (d.getBasicType() != BasicType.INTEGER && d.getBasicType() != BasicType.REAL && d.getBasicType() != BasicType.BOOL)
			exceptionHandler(new  CodeError(operationStr + " is not supported for " + d.getBasicType() + " (only int and real and boolean)", "on line " + scanner.lineNumber()));
	}
	
	private void ensureNonVoid(String s) throws Exception {
    	if (s == "void")
    		throw new CodeError("void is only for return type", "on line " + scanner.lineNumber());
    }
    
    public void finishCode() throws Exception {
    	if(jmpMain == null)
    		exceptionHandler(new  CodeError("main function not found", scanner.lineNumber()+""));
    	instructions.add(0, jmpMain);
    	instructions.add(endInstructions);
    	int line = 0;
    	for (MetaInstruction mIns: instructions) {
    		mIns.setFirstLine(line);
    		line += mIns.getLineCount();
    	}
    }

    public void writeOutput(String os) throws IOException {
    	OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(os));
    	for (MetaInstruction mIns: instructions) {
    		for (Instruction i: mIns.getInstructions()) {
    			String string = i.toInstructionFormat();
    			string = string.replace("\\", "\\\\");
    			string = string.replace("\n", "\\n");
    			string = string.replace("\t", "\\t");
    			string = string.replace("\r", "\\r");
    			w.write(string + "\n");
//    			System.out.println(string);
    		}
    	}
//    	w.write(instructions.get(0).getInstructions().get(0).toInstructionFormat() + "\n"); //**king last jumps
    	w.close();
    }

//	public void debugInfo() {
//		System.out.println("ss: " + sematicStack);
//	}

}
