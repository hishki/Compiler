package compiler.error;

public class CodeError extends MyCompilerError {
	private static final long serialVersionUID = -6132353416705689317L;

	public CodeError(String msg, String location) {
		super("Code Error" + (location != null ? " at " + location : "") + " "+ msg);
	}
}
