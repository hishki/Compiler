package compiler.error;

public class SyntaxError extends MyCompilerError {
	private static final long serialVersionUID = -8326088740781812480L;
	
	public SyntaxError(String msg, String location) {
		super("Syntax Error" + (location != null ? " at " + location : "") + " "+ msg);
	}
}
