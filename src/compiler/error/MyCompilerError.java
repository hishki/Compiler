package compiler.error;

public class MyCompilerError extends Exception {
	private static final long serialVersionUID = 3439940748800517044L;

	public MyCompilerError(String msg) {
		super(msg);
	}
	
	public MyCompilerError() {
	}
}
