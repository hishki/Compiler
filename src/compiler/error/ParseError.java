package compiler.error;

public class ParseError extends MyCompilerError {
	private static final long serialVersionUID = 3116676591034178123L;

	public ParseError(String msg, String location) {
		super("Parse Error" + (location != null ? " at " + location : "") + " "+ msg);
	}
}
