package compiler.parser;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Stack;

import compiler.codegenerator.CodeGenerator;
import compiler.error.CodeError;
import compiler.error.ParseError;
import compiler.scanner.Scanner;
import compiler.scanner.Token;
import compiler.scanner.TokenType;

public class Parser 
{
	Scanner scanner;
	CodeGenerator cg;
	PTBlock[][] parseTable;
	Stack<Integer> parseStack = new Stack<Integer>();
	String[] symbols;
	boolean isInMultiAssign = false;
	Token prevToken = null;


	public Parser(String inputFile, String[] symbols, PTBlock[][] parseTable)
	{
		try
		{
			this.parseTable = parseTable;
			this.symbols = symbols;
	
			scanner = new Scanner(new FileReader(inputFile));
			cg = new CodeGenerator(scanner);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public int LineNumber()
	{
		return scanner.lineNumber(); // Or any other name you used in your Scanner
	}

	public void Parse() throws IOException, ParseError, CodeError, Exception
	{
		Token _token = getNextToken();
		int tokenId = nextTokenID(_token);
		int curNode = 0;
		boolean notAccepted = true;
		while (notAccepted)
		{
			String token = symbols[tokenId];
            PTBlock ptb = parseTable[curNode][tokenId];
			switch (ptb.getAct())
			{
				case PTBlock.ActionType.Error:
					{
						System.err.println("Valid Token list:");
						for (int i = 0; i < symbols.length; i++) {
							if (parseTable[curNode][i].getAct() != PTBlock.ActionType.Error)
								System.err.println(symbols[i] + " " + parseTable[curNode][i]);
						}
						System.err.println();
						throw new ParseError("Unexcpected Token " + token + " @" + curNode, "" + LineNumber());
					}
				case PTBlock.ActionType.Shift:
					{
						cg.codeGenerate(ptb.getSem(), _token);
						prevToken = _token;
						_token = getNextToken();
						tokenId = nextTokenID(_token);
						curNode = ptb.getIndex();
					}
					break;

				case PTBlock.ActionType.PushGoto:
					{
						parseStack.push(curNode);
						curNode = ptb.getIndex();
					}
					break;

				case PTBlock.ActionType.Reduce:
					{
						if (parseStack.size() == 0)
                        {
							throw new ParseError("Unexcpected Token " + token + " @" + curNode, "" + LineNumber());
                        }

						curNode = parseStack.pop();
						ptb = parseTable[curNode][ptb.getIndex()];
						cg.codeGenerate(ptb.getSem(), _token);
						curNode = ptb.getIndex();
					}
					break;

				case PTBlock.ActionType.Accept:
					{
						notAccepted = false;
					}
					break;
					
			}
        }
        cg.finishCode();
	}
	
	Token getNextToken() throws IOException {
		Token token =  scanner.NextToken();
		if(prevToken != null && prevToken.getText().equals("assign") && token.getText().equals("<"))
			isInMultiAssign = true;
		if(isInMultiAssign && token.getText().equals(">"))
		{
			token = new Token(TokenType.OPERATOR, "@@");
			isInMultiAssign = false;
		}
		System.err.println("next Token is: " + token.getText());
		if (token.getType() == TokenType.ID)
			cg.pushId(token.getText());
		return token;
	}

	int nextTokenID(Token token)
	{
		String t = token.getPGenCompatibleString();
		int i;
		
		for (i = 0; i < symbols.length; i++)
			if (symbols[i].equals(t))
				return i;
		(new Exception("Undefined token: " + t)).printStackTrace();
		return 0;
	}
	
	public void WriteOutput(String outputFile) throws IOException // You can change this function, if you think it is not comfortable
	{
        	cg.writeOutput(outputFile);
	}
}


