package compiler;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import compiler.error.ParseError;
import compiler.parser.PTBlock;
import compiler.parser.Parser;

public class Program 
{
	// Address of PGen output table.
	public static final String stPath = "./PGen/pt.npt";
	
	public static String inputPath = "tests/test_exp.Blend";
	public static String outputPath = inputPath.split("\\.(?=[^\\.]+$)")[0] + ".out";
	public static String resPath = inputPath.split("\\.(?=[^\\.]+$)")[0] + ".res";
	public static void main(String[] args)
	{

        if ( args.length != 1)
        {
            System.err.println("Wrong parameters passed.");
            System.err.println("Use the following format:");
            System.err.println("java Program inputfilename.L");
           return;
        }
        else
        {
	        inputPath = args[0];
	        outputPath = inputPath.split("\\.(?=[^\\.]+$)")[0] + ".out";
	        resPath = inputPath.split("\\.(?=[^\\.]+$)")[0] + ".res";
//	        outputPath = args[1];
        }

	String[] symbols = null;
	PTBlock[][] parseTable = null;

        if (!FileExists(stPath) || !FileExists(inputPath))
        {
        	System.out.println("File not found: " + stPath + " or " + inputPath);
            return;
        }

        try
        {
    		int rowSize, colSize;
    		String[] tmpArr;
    		PTBlock block;
    		
    		try
    		{
    			java.io.FileInputStream fis = new java.io.FileInputStream(new java.io.File(stPath));
    			java.util.Scanner sc = new java.util.Scanner(fis);
    			
    			tmpArr = sc.nextLine().trim().split(" ");
    			rowSize = Integer.parseInt(tmpArr[0]);
    			colSize = Integer.parseInt(tmpArr[1]);
    	
    			String SL = sc.nextLine();
    			// This is the line creates an array of symbols depending on the parse table read.
    			symbols = SL.trim().split(" ");
    	
    			parseTable = new PTBlock[rowSize][colSize];
    			for (int i = 0; sc.hasNext(); i++)
    			{
    	
    				if (!sc.hasNext())
    					throw new ParseError("Ivalid .npt file", null);
    	
    				tmpArr = sc.nextLine().trim().split(" ");
    	
    				//PGen generates some unused rows!
    				if (tmpArr.length == 1)
    				{
    					System.out.println("Anomally in .npt file, skipping one line");
    					continue;
    				}
    	
    				if (tmpArr.length != colSize * 3)
    					throw new ParseError("Ivalid line in .npt file", Integer.toString(i));
    					for (int j = 0; j < colSize; j++)
    				{
    					block = new PTBlock();
    					block.setAct(Integer.parseInt((tmpArr[j * 3])));
    					block.setIndex(Integer.parseInt(tmpArr[j * 3 + 1]));
    					block.setSem(tmpArr[j * 3 + 2]);
    					parseTable[i][j] = block;
    				}
    	
    			}
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}

        }
        catch (Exception ex)
        {
            System.out.println("Compile Error -> " + ex.getMessage());
            write_res("0");
            return;
        }

		Parser parser = new Parser(inputPath, symbols, parseTable);

        try
        {
            parser.Parse();
            parser.WriteOutput(outputPath);
            write_res("1");
        }
        catch (Exception ex)
        {
            System.out.println("Compile Error -> " + ex.getMessage());
            write_res("0");
        }
	}

	static void write_res(String res){
		try{
	    	OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(resPath));
	    	w.write(res);
	    	w.close();
	    	if(res.equals("0"))
	    	{
	    		w = new OutputStreamWriter(new FileOutputStream(outputPath));
	    		w.write("");
	    		w.close();
	    	}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}
	static boolean FileExists(String path)
	{
		java.io.File f = new java.io.File(path);
		boolean b = f.exists();
		if (!b)
			System.out.println("ERROR: File not found: {0}" + path);

		return b;
	}

	// You don't need know about the details of this method
	static void LoadPT(String stPath, String[] symbols, PTBlock[][] parseTable)
	{
		int rowSize, colSize;
		String[] tmpArr;
		PTBlock block;
		
		try
		{
			java.io.FileInputStream fis = new java.io.FileInputStream(new java.io.File(stPath));
			java.util.Scanner sc = new java.util.Scanner(fis);
			
			tmpArr = sc.nextLine().trim().split(" ");
			rowSize = Integer.parseInt(tmpArr[0]);
			colSize = Integer.parseInt(tmpArr[1]);
	
			symbols = sc.nextLine().trim().split(" ");
	
			parseTable = new PTBlock[rowSize][colSize];
			for (int i = 0; i < rowSize; i++)
			{
	
				if (!sc.hasNext())
					throw new ParseError("Ivalid .npt file", null);
	
				tmpArr = sc.nextLine().trim().split(" ");
	
				//PGen generates some unused rows!
				if (tmpArr.length == 1)
				{
					System.out.println("Anomally in .npt file, skipping one line");
					continue;
				}
	
				if (tmpArr.length != colSize * 3)
					throw new ParseError("Ivalid line in .npt file", Integer.toString(i));
				for (int j = 0; j < colSize; j++)
				{
					block = new PTBlock();
					block.setAct(Integer.parseInt((tmpArr[j * 3])));
					block.setIndex(Integer.parseInt(tmpArr[j * 3 + 1]));
					block.setSem(tmpArr[j * 3 + 2]);
					parseTable[i][j] = block;
				}
	
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
